# Unique Sequential Identifier (usid)

# About

Generating sequential unique id as short as possible

# Description

When using a unique id to identify data that are kept for a given period of
time, the UUIDv4 is not the most optimum solution. First, it is 36 characters
long for only 122 bits of random data. Second, it is not ordered: insertions
(especially in databases) is much slower.

Snowflake addresses these two points, but it requires a per-module
configuration and a sequence generator.

Using a compound id, based on a time part and a random part, offers the
advantage of the near sequential id, without the problem of the sequence
generator.

KSUID would be a solution but the id is still 27 characters long: it uses
a large time part, and a random part larger than UUIDv4. This sounds like
an overkill since probabilities of collisions are reduced due to the time
prefix.

What we could do is to target the same quality (collision probability) of
UUIDv4 over a given period of time (period after which ids are discarded)
to reduce the needed time and random part sizes.

For example using UUIDv4 over a 2 years period (assuming 100 items
generated per second) has probability of collision of:

    |E|    2^122            population: UUID v4 has 122 random bits
     n     6311520000       number of items in the period (2*365.25*86400*100)
     p     n * n / 2|E|     3.746 E-18

If part of the id uses a time reference (second based), to have the same
probability, the number of random bits can be reduced to:

     p     3.746 E-18        target probability from above
     n     100               number of items in one second
    |E|    n * n / 2 p       1.335 E+21
    bits   log( |E|, 2 )     70.18, rounded to 71

Note that the frequency (number of items generated per second) does
not affect this calculation.

The time part comes first in the id, to have sequentiality. The number of
bits needed to encode the required period (using second) is:

    log( period, 2 )

It is thus possible to build the following tables, per required period.

             |         bits          |
      period | time  random | total  |
      -------|--------------|--------|
        1h   |  12    99    |  111   |
        2h   |  13    97    |  110   |
        4h   |  14    95    |  109   |
        8h   |  15    93    |  108   |
        16h  |  16    91    |  107   |
        1d   |  17    90    |  111   |
        2d   |  18    88    |  106   |
        4d   |  19    86    |  105   |
        7d   |  20    84    |  104   |
        14d  |  21    82    |  103   |
        30d  |  22    80    |  102   |
        60d  |  23    78    |  101   |
        120d |  24    76    |  100   |
        1y   |  25    73    |   98   |
        2y   |  26    71    |   97   |
        4y   |  27    69    |   96   |
        8y   |  28    67    |   95   |
        17y  |  29    65    |   94   |
        34y  |  30    63    |   93   |
        68y  |  31    62    |   93   |

# References

  - https://www.ietf.org/rfc/rfc4122.txt
  - https://developer.twitter.com/en/docs/basics/twitter-ids
  - https://github.com/segmentio/ksuid
  - https://pastebin.com/f66ddcde7
  - https://www.codeproject.com/Tips/785014/UInt-Division-Modulus
  - https://mrob.com/pub/math/int128.c.txt

# License

Copyright 2018 Pierre del Perugia

[Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/)
