# Uniformed JSON response (jresp)

# About

A JSON response format handling non-successful cases.
The existing jsend specification is ambiguous when dealing with errors.
This paper tries to address this.

# Description

When a web service responds to a request, the status of the response
must be relative to the request (what). Moreover the web service must
take into account the context of the invocation (who):

    User -> Interface -> Service -> Resource

User is the human initiating the request, Interface the program used by
the human (web site, smart application...). Resource is an optional
external service (e.g.: airtime top-up, utility bill payment...).

Successful responses are not problematic, there is no ambiguity:

```json
  { "status": "success", "data": { "id": "123456" } }
```

where the content of **data** is protocol dependent. If not used, it must
be set to null.

But there is three error status to handle properly:

- *error*,     when there is an error in the request
- *failed*,    if the Service failed to deliver the service
- *rejected*,  if the parameters or the User's context prevent the service from executing the request

An *error* response means the Interface's developer wrongly implement the
request. The User does not have to know the details: the Interface
should return a generic message to the User. The Service returns a
message targeted to the developer (set to null if not used). Here for
example if a parameter named fields is incorrect:

```json
  { "status": "error", "message": "fields: invalid JSON expression" }
```

A *failed* response means the Service met an internal problem, or one
of the Resource needed by the Service is not available. The User and
Interface do not have to know the details.

```json
  { "status": "failed" }
```

A request can be *rejected* either because the Service decides so (wrong
user's state, rights, balance...) or because a Resource returned a
*failed* status indicating that some of the User's parameters were invalid
(invalid MSISDN number, unknown invoice number...). Beside the **reason**
(of the rejection), the **data** field gives extra indication targeted to
the User. For example:

```json
  { "status": "rejected", "reason": "invalid", "data": "msisdn" }
```    
```json
  { "status": "rejected", "reason": "not_found", "data": "invoice" }
```    
```json
  { "status": "rejected", "reason": "limit", "data": {
      "type":   "count",
      "period": "P7d" } }
```

The protocol defines the list of reasons, suggested ones are:

    exists, incompatible, invalid, limit, locked, not_found, permission

**data** could contain the faulty parameter (or action for permission), or
null. Concerning *limit*, **data** could contain an object with two keys:

1. **type**, one of:
    - *maximum*,  the upper limit for a single transaction
    - *minimum*,  the lower limit for a single transaction
    - *count*,    the number of transactions over a period of time
    - *sum*,      the total sum over a period of time

2. **period**: if **type** is *count* or *sum*, **period** is a Duration
               as defined in RFC 3339, or null otherwise.

Finally the intermediate status *continue* may be used when more data are
needed (used for example when a two factor authentication is needed).

```json
  { "status": "continue", "data": {
      "type": "totp",
      "hint": null } }
```

Content of **data** is protocol dependent: this is an example for a 2FA,
with indication to the Interface that the second factor is a TOTP code
to be retrieved from the User. If not used, it must be set to null.

Note that several Interfaces can be chained between the User and the
Service, or the Service can have several internal layers, but the
response is always from the point of view of the one generating it:
either addressed to the User or to the previous Interface (or layer).
Similarly, Resource is not always an external entity but, from the
Service point of view, is the next layer (or the client invoking the
Resource).

# Syntax summary

```json
  { "status": "success", "data": ... }
```    
```json
  { "status": "error", "message": ... }
```    
```json
  { "status": "failed" }
```
```json
  { "status": "rejected", "reason": ..., "data": ... }
```
```json
  { "status": "continue", "data": ... }
```

# References

  - https://labs.omniti.com/labs/jsend

# License

Copyright 2018 Pierre del Perugia

[Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/)
