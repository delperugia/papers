# Digest signin

# About

Protecting user credentials at all times

# Description

It seems to be commonly accepted nowadays to convey user password
unencrypted over a TLS connection. However even if a hash of the password
is stored on the server, the plain password can be intercept on the server,
after exiting the TLS connection. For example in the HTTP server logs, in
the server RAM or even in debug log. Recently GitHub (2018-05-01) and
Twitter (2018-05-04) announced that such a thing occurred (plain passwords
were stored in system logs). That's also one point Eran Hammer (the designer
of OAuth 1) reproaches to OAuth 2: relying on the TLS transport to secure
user credentials. In numerous banking contexts, conveying a credit card
PIN is forbidden, even over TLS.

The only way to guarantee that a user's password does not leak from
a server is if this server does not receive it. The following flows do
this. There is some similitudes with Digest access authentication.

Here the server only keep for each user a __salt__ value and a __digest__,
and during check phases a __nonce__. The database structure would look like:

 
 | user_id | digest    | salt      | nonce |
 |---------|-----------|-----------|-------|
 | 123...  | 8eE49a... | 79eAd1... |       |

## Flow to set a password

    SET(P)
        set_1 ->                        generate and store salt
                <- salt                 return salt
        set_2 ->   H(P,salt)            store as digest

## Flow to check password

    CHECK(P)
        chk_1 ->                        generate and store nonce
                <- salt, nonce          return nonce and stored salt
        chk_2 ->   H(H(P,salt),nonce)   is this equal to H(digest,nonce)?

## Flow to change a password

    CHANGE(Po,Pn)
        chg_1 ->                        generate and store nonce
                <- salt, nonce          return nonce and stored salt
        chk_2 ->   H(H(Po,salt),nonce)  is this equal to H(digest,nonce)?
                   H(Pn,nonce)          store as new digest, store nonce as new salt

## Comments

salt and nonce: 128 bits random data

Po, Pn: old password, new password

H(P,S): PBKDF2( HMAC-SHA256, P, S, 100000 iterations, 128 bits )

# Afterthoughs

Exposing the salt may not be a good idea: it may allow a hacker
to prepare a rainbow table for these salts. If he can then access
the stored digests, he could quickly found the passwords.

Using this method only to prevent password storage is not a good
idea: others information must never be stored (PIN...). Using
it for all sensitive information may prove too complex, a better
solution should be found if the goal is only to prevent storage.

# License

Copyright 2018 Pierre del Perugia

[Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/)
